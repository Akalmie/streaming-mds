<?php
require "backend/init.php";
/*if (isset($_SESSION['userLoggedIn'])) {
  $user_id = $_SESSION['userLoggedIn'];
  Login::isLoggedIn();
  //echo $user_id;
} else {
  redirect_to('index.php');
}*/

require "backend/shared/header.php";
?>

<body class="film-page">

  <nav>
    <img src="frontend/assets/images/Netflix_Logo_RGB.png">
    <div class="nav-bar">
      <a href="">HOME</a>
      <a href="">Séries</a>
      <a href="">Films</a>
    </div>
  </nav>
  <div id="video">
    <video autoplay muted>
      <source src="frontend/assets/videos/casa-de-papel.mp4" type="video/mp4">
    </video>
  </div>

  <section class="content">
    <h2>My list</h2>
    <div class="row space-between">
      <img src="frontend/assets/images/thumbnails/thumb-1.jpg">
      <img src="frontend/assets/images/thumbnails/thumb-2.jpg">
      <img src="frontend/assets/images/thumbnails/thumb-3.jpg">
      <img src="frontend/assets/images/thumbnails/thumb-4.jpg">
    </div>
    <div class="row space-between">
      <img src="frontend/assets/images/thumbnails/thumb-5.jpg">
      <img src="frontend/assets/images/thumbnails/thumb-6.jpg">
      <img src="frontend/assets/images/thumbnails/thumb-7.jpg">
      <img src="frontend/assets/images/thumbnails/thumb-8.jpg">
    </div>
  </section>
  <section class="content">
    <div class="row">
      <div>
        <video autoplay muted>
          <source src="frontend/assets/videos/elite.mp4" type="video/mp4">
        </video>
      </div>
      <div class="textvideo">
        <h2> #1 chez les adolescents mentaux</h2>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatum hic totam perspiciatis repudiandae, natus quisquam, eaque ad labore iste numquam enim vero, rem velit ipsa quo ut ea animi! Aut?
          Nemo sequi praesentium tempora nesciunt repellat quo quidem necessitatibus quasi at debitis totam quas odio a, illo sint dolorem ad provident laboriosam rerum reprehenderit impedit est corrupti? Animi, eaque itaque.
          Culpa non consectetur debitis ab dolor dolorum. Expedita fugit suscipit magnam ea vero. Nam ad quibusdam ex incidunt exercitationem, qui voluptatum ab, culpa modi vel totam possimus, velit consequuntur ipsa.
          Excepturi beatae similique qui fugiat dolorum voluptatem necessitatibus veritatis laboriosam, deleniti harum vero non libero saepe et, impedit illum consequatur doloribus repellendus culpa adipisci recusandae quas? Dolorum rerum repudiandae velit.
        </p>
        <button>Play</button>
        <button>More info</button>
      </div>
    </div>

  </section>

  <section class="content">
    <h2>My list</h2>
    <div class="row space-between">
      <img src="frontend/assets/images/thumbnails/thumb-9.jpg">
      <img src="frontend/assets/images/thumbnails/thumb-10.jpg">
      <img src="frontend/assets/images/thumbnails/thumb-8.jpg">
      <img src="frontend/assets/images/thumbnails/thumb-7.jpg">
    </div>
    <div class="row space-between">
      <img src="frontend/assets/images/thumbnails/thumb-6.jpg">
      <img src="frontend/assets/images/thumbnails/thumb-5.jpg">
      <img src="frontend/assets/images/thumbnails/thumb-4.jpg">
      <img src="frontend/assets/images/thumbnails/thumb-3.jpg">
    </div>
  </section>


</body>