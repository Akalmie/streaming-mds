<?php

class User{
    private $pdo;

    public function __construct(){
        $this->pdo=Database::instance();
    }
    public function create($tableName,$fields=array()){
      $column=implode(', ' ,array_keys($fields));
      $values=implode(', :' ,array_keys($fields));
      $sql='INSERT INTO {$tableName}({$column} VALUES {$values})';
      if($stmt=$this->pdo->prepare($sql)){
        foreach($fields as $key=> $values){
        $stmt->bindValue(": " .$key , $values);
      }
   
      $stmt->execute();
      return $this->pdo->lastInsertId();
    }
    }
  }
