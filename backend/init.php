<?php
ob_start();

require "backend/config.php";

spl_autoload_register(function ($class) {
  require_once "classes/{$class}.php";
});

try {
  $dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
} catch (PDOException $e) {
  echo 'fail' . $e->getMessage();
}

session_start();
require "functions.php";
$loadFromUser = new User;
$account = new Account;
